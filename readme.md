**Peru APPs**

**Pasos:**

Instalar dependencias usando composer
```sh
composer install
```

Crear archivo .env y agregar los datos necesarios para el proyecto, como conexión a base de datos y correo del cual se enviaran los correos de las notificaciones, usar el archivo _**.env.example**_ como ejemplo

Crear APP_KEY
```sh
php artisan key:generate
```

Ejecutar migraciones
```sh
php artisan migrate
```

Instalación y generación de las llaves
```sh
php artisan passport:install
```
**Pruebas**

Usar la colección de postamn incluida en el proyecto en el archivo _**Peru Apps.postman_collection.json**_

Usar como servidor de pruebas _**http://45.33.21.87/peruapps**_