<?php

namespace App\Http\Controllers;

use App\Librerias\Libreria;
use App\Notifications\SignupActivate;
use App\User;
use Avatar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $filas = Libreria::getParam($request->input('filas'));
        $nombre = Libreria::getParam(Libreria::getParam($request->input('nombre')));
        $usuarios = User::listar($nombre)->paginate($filas);
        return response()->json($usuarios);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Empresa $empresa
     * @return void
     */
    public function show($id)
    {
        $usuario = User::find($id);
        return response()->json($usuario);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'name' => 'required|string',
                'email' => [
                    'required',
                    'string',
                    'email',
                    Rule::unique('users')->whereNull('deleted_at'),
                ],
                'password' => 'required|string|confirmed',
                'image' => 'nullable|image',
            )
        );

        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }

        $error = DB::transaction(function () use ($request) {
            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'activation_token' => str_random(60),
            ]);
            $user->save();
            if ($request->archivo) {
                $request->archivo->storeAs('avatars/' . $user->id, $request->archivo->getClientOriginalName());
                $user->avatar = $request->archivo->getClientOriginalName();
                $user->save();
            } else {
                $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
                Storage::put('avatars/' . $user->id . '/avatar.png', (string)$avatar);
            }
            $user->notify(new SignupActivate($user));
        });
        return is_null($error) ? response()->json(['message' => 'Usuario creado existosamente!'], 201) : $error;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validacion = Validator::make($request->all(),
            array(
                'name' => 'required|string',
                'email' => [
                    'required',
                    'string',
                    'email',
                    Rule::unique('users')->whereNull('deleted_at')->ignore($id),
                ],
                'password' => 'required|string|confirmed',
                'image' => 'nullable|image',
            )
        );

        if ($validacion->fails()) {
            return response()->json($validacion->messages());
        }

        $error = DB::transaction(function () use ($request, $id) {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
            if ($request->archivo) {
                $request->archivo->storeAs('avatars/' . $user->id, $request->archivo->getClientOriginalName());
                $user->avatar = $request->archivo->getClientOriginalName();
                $user->save();
            } else {
                $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
                Storage::put('avatars/' . $user->id . '/avatar.png', (string)$avatar);
            }
        });
        return is_null($error) ? response()->json(['message' => 'Usuario actualizado existosamente!'], 200) : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $error = DB::transaction(function () use ($id) {
            $empresa = User::find($id);
            $empresa->delete();
        });
        return is_null($error) ? response()->json(['message' => 'Usuario eliminado existosamente!'], 200) : $error;
    }
}
